Vue 3 / vitest basic example
============================

Extracted from: https://github.com/vitest-dev/vitest/tree/main/examples/vue

# Prerequisites

Nodejs >= 14

# Install

    yarnpkg

On Debian stable (bullseye):

    docker run -it --rm -v "$PWD:/app" -p 5173:5173 -w /app node:16 yarnpkg

# Run tests

    yarnpkg test

On Debian stable (bullseye):

    docker run -it --rm -v "$PWD:/app" -p 5173:5173 -w /app node:16 yarnpkg test